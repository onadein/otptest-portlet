create role otptest;
alter role otptest with nosuperuser inherit nocreaterole nocreatedb login password 'q1w2e3' valid until 'infinity';

CREATE DATABASE otptest owner = otptest;

\connect otptest

CREATE TABLE data
(
  id bigint NOT NULL,
  value character varying(255),
  CONSTRAINT pk_data_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE data OWNER TO otptest;

CREATE TABLE info
(
  id bigint NOT NULL,
  value character varying(255),
  data_id bigint,
  CONSTRAINT pk_info_id PRIMARY KEY (id),
  CONSTRAINT fk_dataid_data FOREIGN KEY (data_id)
      REFERENCES data (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE info OWNER TO otptest;

CREATE INDEX fki_dataid_data ON info USING btree (data_id);

--// Some data for test

INSERT INTO data (id, value) VALUES (1, '2');
INSERT INTO data (id, value) VALUES (2, '3');
INSERT INTO data (id, value) VALUES (3, '4');
INSERT INTO data (id, value) VALUES (4, '5');

INSERT INTO info (id, value, data_id) VALUES (1, '2', 1);
INSERT INTO info (id, value, data_id) VALUES (2, '3', 1);
INSERT INTO info (id, value, data_id) VALUES (3, '4', 2);
INSERT INTO info (id, value, data_id) VALUES (4, '5', 2);
INSERT INTO info (id, value, data_id) VALUES (6, '7', 3);
INSERT INTO info (id, value, data_id) VALUES (5, '6', 3);
