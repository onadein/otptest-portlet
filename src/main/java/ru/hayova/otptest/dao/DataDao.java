package ru.hayova.otptest.dao;

// (c) Oleg 'hayova' Nadein, 2016

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import ru.hayova.otptest.common.Defs;
import ru.hayova.otptest.model.Info;

public class DataDao {
	
	@SuppressWarnings("unchecked")
	public static List<Info> listDataByIds(long lineNumber1, long lineNumber2) {
		
		EntityManager em = LocalEntityManagerFactory.createEntityManager();
		
		EntityTransaction et = em.getTransaction();
        et.begin();
        
		List<Info> list = em.createQuery(Defs.QUERY_LIST_INFO)
	                .setParameter(Defs.QUERY_PARAM_ID, lineNumber2)
	                .setParameter(Defs.QUERY_PARAM_DATAID, lineNumber1)
	                .getResultList();
		
		et.commit();
        em.close();
        
		return list;
	}
}
