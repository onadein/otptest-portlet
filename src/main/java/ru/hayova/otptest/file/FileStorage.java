package ru.hayova.otptest.file;

// (c) Oleg 'hayova' Nadein, 2016

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import ru.hayova.otptest.common.Defs;

public class FileStorage {
    private static final FileStorage instance = new FileStorage();

    private FileStorage() {
        super();
    }

    public synchronized void writeToFile(String filename, String inputField1, String inputField2) throws IOException {
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
			if(inputField1 != null && !inputField1.isEmpty())
				out.println(inputField1);
			if(inputField2 != null && !inputField2.isEmpty())
				out.println(inputField2);
		}catch (IOException e) {
			throw new IOException(e);
		}
    }
    
	public synchronized String readLinesFromFile(String filename, Integer lineNumber1, Integer lineNumber2) {
		String result = "";
		String line;
		Integer lineCount = 0;
		try (
		    InputStream fis = new FileInputStream(filename);
		    InputStreamReader isr = new InputStreamReader(fis, Charset.forName(Defs.CHARSET_UTF8_NAME));
		    BufferedReader br = new BufferedReader(isr);
		) {
			while ((line = br.readLine()) != null) {
				if(lineCount == lineNumber1)
					result += line + " ";
				if(lineCount == lineNumber2)
					result += line + " ";
				lineCount++;
				if(lineCount > (lineNumber1 > lineNumber2 ? lineNumber1 : lineNumber2))
					break;
		    }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result.trim();
	}

    public static FileStorage getInstance() {
        return instance;
    }

}