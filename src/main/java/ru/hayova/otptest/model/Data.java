package ru.hayova.otptest.model;

// (c) Oleg 'hayova' Nadein, 2016

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "data", schema = "public")
public class Data implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "value")
	private String value;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "data")
	private Set<Info> infos = new HashSet<Info>(0);

	public Data() {
	}

	public Data(long id) {
		this.id = id;
	}

	public Data(long id, String value, Set<Info> infos) {
		this.id = id;
		this.value = value;
		this.infos = infos;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Set<Info> getInfos() {
		return this.infos;
	}

	public void setInfos(Set<Info> infos) {
		this.infos = infos;
	}

}
