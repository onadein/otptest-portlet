package ru.hayova.otptest.common;

// (c) Oleg 'hayova' Nadein, 2016

public class Defs {
	public final static String PERSISTENCE_UNIT_NAME = "otptest";
	
	public final static String LOG_FILE_NAME = "/data.log";
	
	public final static String MESSAGE_ERROR_SAVING = "ERROR SAVING DATA";
	public final static String MESSAGE_SUCCESS_SAVE = "SUCCESSFULLY SAVED";
	
	public final static String QUERY_LIST_INFO = "SELECT i FROM Info i WHERE id = :id AND data_id = :data_id";
	public final static String QUERY_PARAM_ID = "id";
	public final static String QUERY_PARAM_DATAID = "data_id";
	
	public final static String CHARSET_UTF8_NAME = "UTF-8";
}
