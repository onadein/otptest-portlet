package ru.hayova.otptest.portlet;

// (c) Oleg 'hayova' Nadein, 2016

import java.io.IOException;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import ru.hayova.otptest.common.Defs;
import ru.hayova.otptest.dao.DataDao;
import ru.hayova.otptest.file.FileStorage;
import ru.hayova.otptest.model.Info;

public class OtpTest extends MVCPortlet {
	

	@Override
	public void serveResource(ResourceRequest resourceRequest,
	        ResourceResponse resourceResponse) throws IOException,
	        PortletException {

		String cmd = ParamUtil.getString(resourceRequest, Constants.CMD);

		if (cmd.equals("processCalc1")) {
			String inputField1 = ParamUtil.getString(resourceRequest, "inputField1");
			String inputField2 = ParamUtil.getString(resourceRequest, "inputField2");

			resourceResponse.getWriter().write(processCalc1(inputField1, inputField2));
		} else if (cmd.equals("processCalc2")) {
			String inputField3 = ParamUtil.getString(resourceRequest, "inputField3");
			String inputField4 = ParamUtil.getString(resourceRequest, "inputField4");

			resourceResponse.getWriter().write(processCalc2(inputField3, inputField4));
		} else if (cmd.equals("processSave")) {
			String inputField1 = ParamUtil.getString(resourceRequest, "inputField1");
			String inputField2 = ParamUtil.getString(resourceRequest, "inputField2");
			
			resourceResponse.getWriter().write(processSave(inputField1, inputField2));
		}
	
	}
	
	public String processCalc1(String inputField1, String inputField2) {
		Integer lineNumber1 = 0;
		Integer lineNumber2 = 0;
		try {
			lineNumber1 = Integer.parseInt(inputField1);
		} catch (NumberFormatException e) {}
		
		try {
			lineNumber2 = Integer.parseInt(inputField2);
		} catch (NumberFormatException e) {}
		
		List<Info> list = DataDao.listDataByIds(lineNumber1, lineNumber2);

	    Integer summ = 0;

		if(list.size() > 0) {
		    Integer fieldValue = 0;
		    
			if(list.size() > 0) {
				for(Info info : list) {
					fieldValue = 0;
					try {
						fieldValue = Integer.parseInt(info.getValue());
					} catch (NumberFormatException e) {}
					summ += fieldValue;
				}
			}
		}
		
		return String.valueOf(summ);
	}

	public String processCalc2(String inputField3, String inputField4) {
		Integer lineNumber1 = 0;
		Integer lineNumber2 = 0;
		try {
			lineNumber1 = Integer.parseInt(inputField3);
		} catch (NumberFormatException e) {}
		
		try {
			lineNumber2 = Integer.parseInt(inputField4);
		} catch (NumberFormatException e) {}
		
		return FileStorage.getInstance().readLinesFromFile(getPortletContext().getRealPath(Defs.LOG_FILE_NAME), lineNumber1, lineNumber2);
	}
	
	public String processSave(String inputField1, String inputField2) {
		try {
			FileStorage.getInstance().writeToFile(getPortletContext().getRealPath(Defs.LOG_FILE_NAME), inputField1, inputField2);
			return inputField1 + " " + inputField2 + " " + Defs.MESSAGE_SUCCESS_SAVE;
		} catch (IOException e) {
			e.printStackTrace();
			return Defs.MESSAGE_ERROR_SAVING;
		}
	}

	
}
