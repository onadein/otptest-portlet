<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<portlet:defineObjects />

<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.Constants"%>

<portlet:resourceURL var="processSaveResourceURL">
  <portlet:param name="<%= Constants.CMD %>" value="processSave" />
</portlet:resourceURL>

<portlet:resourceURL var="processCalc1ResourceURL">
  <portlet:param name="<%= Constants.CMD %>" value="processCalc1" />
</portlet:resourceURL>

<portlet:resourceURL var="processCalc2ResourceURL">
  <portlet:param name="<%= Constants.CMD %>" value="processCalc2" />
</portlet:resourceURL>

<script type="text/javascript">
	function clearInfo() {
		AUI().one("#<portlet:namespace />outputField3").set('text', '');
	}
</script>

<aui:script use="aui-io-request,aui-node">
	Liferay.provide(window,'processCalc1',
		function() {
			var A = AUI();
				A.io.request('<%=processCalc1ResourceURL%>',{
				method: 'POST',
				form: { id: '<portlet:namespace/>actionUrl' },
				on: {
				success: function(){
					var data=this.get('responseData');
		    		A.one("#<portlet:namespace />outputField1").set('text', data);
				}
			}
		});
	}); 
	Liferay.provide(window,'processCalc2',
		function() {
			var A = AUI();
				A.io.request('<%=processCalc2ResourceURL%>',{
				method: 'GET',
				form: { id: '<portlet:namespace/>actionUrl' },
				on: {
				success: function(){
					var data=this.get('responseData');
					A.one("#<portlet:namespace />outputField2").set('text', data);
				}
			}
		});
	});  
	Liferay.provide(window,'processSave',
		function() {
			var A = AUI();
				A.io.request('<%=processSaveResourceURL%>',{
				method: 'POST',
				form: { id: '<portlet:namespace/>actionUrl' },
				on: {
				success: function(){
					var data=this.get('responseData');
					A.one("#<portlet:namespace />outputField3").set('text', data);
				}
			}
		});
	});  
</aui:script>

<aui:form method="POST" action="javascript:void();" name="actionUrl" id="actionUrl" >
	<table border="1"> 
		<tr>
			<td>Caption 1</td>
			<td>Caption 2</td>
			<td>Caption 3</td>
			<td></td>
		</tr>
		<tr>
			<td><aui:input name="inputField1" id="inputField1" type="text" label="<%= StringPool.BLANK %>" /></td>
			<td><aui:input name="inputField2" id="inputField2" type="text" label="<%= StringPool.BLANK %>" /></td>
			<td><div id="<portlet:namespace />outputField1" ></div></td>
			<td><aui:button onClick="clearInfo();processCalc1();" value="calculate-1"/></td>
		</tr>
		<tr>
			<td><aui:input name="inputField3" type="text" label="<%= StringPool.BLANK %>" /></td>
			<td><aui:input name="inputField4" type="text" label="<%= StringPool.BLANK %>" /></td>
			<td><div id="<portlet:namespace />outputField2" ></div></td>
			<td><aui:button onClick="clearInfo();processCalc2();" value="calculate-2"/></td>
		</tr>
		<tr>
			<td colspan="3"><div id="<portlet:namespace />outputField3" ></div></td>
			<td><aui:button onClick="clearInfo();processSave();" value="save"/></td>
		</tr>
	</table>
</aui:form>
